import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PromotionalPageComponent } from './promotional-page/promotional-page.component';
import { ShoppingPageComponent } from './shopping-page/shopping-page.component';
import { ProductDetailPageComponent } from './product-detail-page/product-detail-page.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    TopBarComponent,
    PromotionalPageComponent,
    ShoppingPageComponent,
    ProductDetailPageComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [NgbActiveModal],
  bootstrap: [AppComponent]
})
export class AppModule { }
