export interface Product {
    id: number;
    name: string;
    uom: string
    price: number;
    description: string;
    image: string;
}

export const products = [
    {
        id: 1,
        name: 'Ripe Blue Grape',
        uom: 'bundle',
        price: 799,
        description: 'Lorem ipsum sit dolor',
        image: 'assets/images/shopping-page/untitled-1.png'
    },
    {
        id: 2,
        name: 'Spinach',
        uom: 'bundle',
        price: 699,
        description: 'Lorem ipsum sit dolor',
        image: 'assets/images/shopping-page/untitled-2.png'
    },
    {
        id: 3,
        name: 'Salmon',
        uom: 'bundle',
        price: 299,
        description: 'Lorem ipsum sit dolor',
        image: 'assets/images/shopping-page/untitled-3.png'
    },
    {
        id: 4,
        name: 'Tomato',
        uom: 'bundle',
        price: 299,
        description: 'Lorem ipsum sit dolor',
        image: 'assets/images/shopping-page/untitled-4.png'
    }
];
