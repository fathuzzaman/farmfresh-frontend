import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductDetailPageComponent } from '../product-detail-page/product-detail-page.component';
import { Product, products } from './products';

@Component({
  selector: 'app-shopping-page',
  templateUrl: './shopping-page.component.html',
  styleUrls: ['./shopping-page.component.css']
})
export class ShoppingPageComponent implements OnInit {
  products = products;
  selectedId?: number | null;

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  seeDetail(id: number) {
    this.selectedId = id;
    this.open(id);
  }

  open(id: number) {
    const modalRef = this.modalService.open(ProductDetailPageComponent);
    modalRef.componentInstance.id = id;
  }

}
