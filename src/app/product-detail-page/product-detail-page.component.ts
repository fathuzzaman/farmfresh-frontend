import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product, products } from '../shopping-page/products';

@Component({
  selector: 'app-product-detail-page',
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.css']
})
export class ProductDetailPageComponent implements OnInit {
  @Input() id?: number | null;
  product?: Product | null;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.product = products.find(x => x.id == this.id);
  }

  closeModal() {
    // this.id = null;
    // this.product = null;
    this.activeModal.close();
    // var modal = document.getElementById("modalProduct");
    // if (modal != null)
    //   modal.style.display = "none";
  }
}
